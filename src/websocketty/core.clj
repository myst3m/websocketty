(ns websocketty.core
  (:gen-class)
  (:require [immutant.web :as web]
            [immutant.web.async :as async]
            [clojure.string :as str])
  (:require [clojure.reflect :as r])
  (:require [clojure.tools.cli :refer [parse-opts]]
            [clojure.data.json :as json]
            [taoensso.timbre :as log]
            [compojure.core :refer :all]
	    [compojure.route :as route]
            [hiccup.core :refer :all])
  (:require [ring.middleware.keyword-params :refer [wrap-keyword-params]]
            [ring.middleware.content-type :refer [wrap-content-type]]
            [ring.middleware.defaults :refer [site-defaults api-defaults wrap-defaults]]))


(def clients (atom {}))
(defonce server (atom nil))

(defn -channel-id [ch]
  (last (str/split (str ch) #"@")))

(defn websocket-manage [req]
  (async/as-channel req
                    {:on-open (fn [ch]
                                (let [id (-channel-id ch)]
                                  (when (@clients id)
                                    (async/close (clients (str ch)))
                                    (swap! clients dissoc id))
                                  (swap! clients conj {id ch} )))
                     :on-error (fn [ch throwable]
                                 (println throwable))
                     :on-message (fn [ch data]
                                   (println data))
                     :on-close (fn [ch {:keys [code reason]}]
                                 (swap! clients dissoc (-channel-id ch)))}))



(defn page [req]
  (html [:div
         [:div (if (seq @clients) "Clients" "No Clients")]
         (for [c (keys @clients)]
           [:form {:method "POST" :action "/" :name "xform" :onsubmit "clickEvent(); return false;"}
            [:table
             [:tr
              [:td c]
              [:td [:input {:type "hidden" :name "connection" :value c}]]
              [:td [:input {:type "text" :name "text"}]]
              [:td [:input {:type "submit" :value "Send"}]] ]]])
         [:script "function clickEvent() {document.xform.submit(); return false;}"]]))

(defroutes url-manage
  (GET "/" [] page)
  (GET "/clients" [] (fn [_] (str (str/join "\n" (keys @clients)) "\n")))
  (POST "/" [] (fn [req]
                 (log/debug req)
                 (let [_ (log/info (:body req))
                       {:keys [connection text]} (:params req)
                       ch (@clients connection)]
                   (when ch (async/send! ch text))
                   
                   {:status 200
                    :headers {"Content-Type" "text/html; charset=utf-8"}
                    :body (page req)})))
  (route/not-found "<h1>Page not found</h1>"))


(defn bootstrap [& [{:keys [port]}]]
  (reset! server (-> {:port port :host "0.0.0.0"}
                     (assoc :path "/")
                     (->> (web/run (-> url-manage
                                       (wrap-defaults
                                        (-> site-defaults
                                            (assoc-in [:security :anti-forgery] false))))))
                     (assoc  :path "/ws")
                     (->> (web/run (-> websocket-manage
                                       (wrap-defaults api-defaults)))))))

(defn -stop []
  (web/stop @server)
  (reset! server nil))

(defn -restart []
  (-stop)
  (bootstrap {:port 4000}))


(def options-schema
  [["-p" "--port NUMBER" "Port number listening on this host"
   :default 4000
   :parse-fn #(Integer/parseInt %)]])

(defn -main [& args]

  (let [{:keys [options]} (parse-opts args options-schema)]
    (log/info "boot options: " options)
    (bootstrap options)))
