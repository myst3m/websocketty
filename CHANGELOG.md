# Change Log
All notable changes to this project will be documented in this file. This change log follows the conventions of [keepachangelog.com](http://keepachangelog.com/).

## 0.1.0 - 2018-05-26
### Initial Release
- Implement fundamental functions

[Unreleased]: https://github.com/your-name/websocketty/compare/0.1.1...HEAD
[0.1.0]: https://github.com/your-name/websocketty/compare/0.1.0...0.1.1
