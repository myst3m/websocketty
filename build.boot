(def project 'websocketty)
(def version "0.1.0")

(set-env! :resource-paths #{"resources" "src"}
          :source-paths   #{"test"}
          :dependencies   '[[org.clojure/clojure "RELEASE"]
                            [org.clojure/data.json "0.2.6"]
                            [adzerk/boot-test "RELEASE" :scope "test"]
                            [org.clojure/tools.cli "0.3.7"]
                            [com.taoensso/timbre "4.10.0"]
                            [compojure "1.6.1"]
                            [ring/ring-defaults "0.3.2"]
                            [org.immutant/web "2.1.10"
                             ;;:exclusions [ch.qos.logback/logback-classic]
                             ]
                            [hiccup "1.0.5"]])

(task-options!
 pom {:project     project
      :version     version
      :description "Bridge between WebSocket from client and HTTP"
      :url         "http://theorems.co/"
      :scm         {:url "https://github.com/yourname/websocketty"}
      :license     {"Eclipse Public License"
                    "http://www.eclipse.org/legal/epl-v10.html"}}
 jar :main websocketty.core)

(deftask build []
  (comp (aot :all true) (pom) (uber)  (jar :main 'websocketty.core :file (str "websocketty-" version ".jar" )) (target)))



(require '[adzerk.boot-test :refer [test]])
